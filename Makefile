# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: glodi <glodistudent.42.fr>                +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/11/23 16:07:28 by glodi             #+#    #+#              #
#    Updated: 2018/12/11 19:03:57 by glodi            ###   ########.fr        #
#                                                                              #
# **************************************************************************** #


# Variables
NAME = libftprintf.a

LIBFT = libft

DIR_SRCS = printf/srcs/
DIR_OBJS = printf/objs/
DIR_INCS = printf/incs/

FILES_PRINTF = ft_printf.c ft_vfprintf.c parser.c to_buffer.c convert.c dec.c \
			   udec.c oct.c hex.c str.c char.c ptr.c float.c pf_lltoa_base.c \
			   pf_ulltoa_base.c  utils.c fill_field_width.c

SRCS_PRINTF = $(addprefix $(DIR_SRCS), $(FILES_PRINTF))
OBJS_PRINTF = $(addprefix $(DIR_OBJS), $(FILES_PRINTF:.c=.o))

CC = gcc
CFLAGS = -Wall -Wextra -Werror -Ofast

INC_COMP = -I$(DIR_INCS) -I$(LIBFT)

AR = ar rc
RAN = ranlib

RM = rm -f
RM_DIR = rm -rf

MAKE = make -C

# Rules
all: $(NAME)

$(DIR_OBJS)%.o: $(DIR_SRCS)%.c
	$(CC) $(CFLAGS) -o $@ -c $< $(INC_COMP)

$(DIR_OBJS):
	mkdir -p $@

$(NAME): $(DIR_OBJS) $(OBJS_PRINTF)
	$(MAKE) $(LIBFT)
	cp libft/libft.a ./$(NAME)
	$(AR) $@ $(OBJS_PRINTF) 
	$(RAN) $@

clean:
	$(MAKE) $(LIBFT) clean
	$(RM_DIR) $(DIR_OBJS)
	
fclean: clean
	$(MAKE) $(LIBFT) fclean
	$(RM) $(NAME)

re: fclean all

test: $(NAME)
	$(CC) $(CFLAGS) -g main.c $(NAME) $(INC_COMP)

norme: $(DIR_INCS) $(DIR_SRCS)
	norminette $^ | grep -B 1 "Warning\|Error" || true

.PHONY: all clean fclean re norme test
