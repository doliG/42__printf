/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: glodi <glodi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/01 14:29:39 by glodi             #+#    #+#             */
/*   Updated: 2017/12/08 13:24:09 by glodi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strdup(const char *s)
{
	int		s_len;
	char	*res;

	s_len = ft_strlen(s);
	if (!(res = (char *)malloc((s_len + 1) * sizeof(char))))
		return (NULL);
	return (ft_strcpy(res, s));
}
