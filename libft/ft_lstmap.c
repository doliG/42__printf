/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: glodi <glodi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/09 11:15:55 by glodi             #+#    #+#             */
/*   Updated: 2017/12/09 15:10:51 by glodi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list *head_newlst;
	t_list *new_el;
	t_list *previous;

	if (!lst || !f)
		return (NULL);
	new_el = f(lst);
	head_newlst = new_el;
	while (lst->next)
	{
		lst = lst->next;
		previous = new_el;
		new_el = f(lst);
		previous->next = new_el;
	}
	return (head_newlst);
}
