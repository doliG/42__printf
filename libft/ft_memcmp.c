/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: glodi <glodi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/02 11:53:41 by glodi             #+#    #+#             */
/*   Updated: 2017/12/09 14:04:33 by glodi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_memcmp(const void *s1, const void *s2, size_t n)
{
	unsigned char *mem1;
	unsigned char *mem2;

	if ((!s1 && !s2) || n == 0)
		return (0);
	mem1 = (unsigned char *)s1;
	mem2 = (unsigned char *)s2;
	while ((n-- > 1) && (*mem1 == *mem2))
	{
		mem1++;
		mem2++;
	}
	return (*mem1 - *mem2);
}
