/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: glodi <glodi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/08 19:46:29 by glodi             #+#    #+#             */
/*   Updated: 2017/12/08 20:11:31 by glodi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list			*ft_lstnew(void const *content, size_t content_size)
{
	t_list *element;

	if (!(element = malloc(sizeof(t_list))))
		return (NULL);
	if (content)
	{
		if (!(element->content = ft_memalloc(content_size)))
			return (NULL);
		ft_memcpy(element->content, content, content_size);
		element->content_size = content_size;
	}
	else
	{
		element->content = NULL;
		element->content_size = 0;
	}
	element->next = NULL;
	return (element);
}
