/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: glodi <glodi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/24 10:48:36 by glodi             #+#    #+#             */
/*   Updated: 2017/12/08 13:23:01 by glodi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strcpy(char *dest, const char *src)
{
	int i;
	int src_len;

	src_len = ft_strlen(src);
	i = 0;
	while (i <= src_len)
	{
		dest[i] = src[i];
		i++;
	}
	return (dest);
}
