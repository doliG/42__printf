/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_convert_base.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: glodi <glodi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/15 12:44:51 by glodi             #+#    #+#             */
/*   Updated: 2017/12/09 17:10:32 by glodi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	to_base_10(char *nbr, char *b_from, int b_length, int nbr_length)
{
	int	calculated_pow;
	int index;
	int	p;

	if (nbr_length < 1)
		return (0);
	p = nbr_length - 1;
	calculated_pow = 1;
	while (p > 0)
	{
		calculated_pow *= b_length;
		p--;
	}
	index = -1;
	while (b_from[++index])
		if (b_from[index] == *nbr)
			break ;
	return (index * calculated_pow
		+ to_base_10(nbr + 1, b_from, b_length, nbr_length - 1));
}

static char	*to_base_xx(int nbr, char *base_to, char *res)
{
	int base_size;

	base_size = 0;
	while (base_to[base_size])
		base_size++;
	if (nbr >= 1)
	{
		to_base_xx(nbr / base_size, base_to, res + 1);
		*res = base_to[nbr % base_size];
	}
	else
		*res = '\0';
	return (res);
}

static int	is_input_valid(char *nbr, char *base_from, char *base_to)
{
	int	i;
	int	j;
	int	is_nb_found;

	i = -1;
	while (nbr[++i] && (j = -1) > -2)
	{
		is_nb_found = 0;
		while (base_from[++j])
			if (nbr[i] == base_from[j])
				is_nb_found = 1;
		if (!is_nb_found)
			return (0);
	}
	i = -1;
	while ((base_to[++i] && (j = i) != -2))
		while (base_to[++j])
			if (base_to[i] == base_to[j])
				return (0);
	i = -1;
	while ((base_from[++i] && (j = i) != -2))
		while (base_from[++j])
			if (base_from[i] == base_from[j])
				return (0);
	return (1);
}

char		*ft_convert_base(char *nbr, char *base_from, char *base_to)
{
	int		b_l;
	int		nb_l;
	int		is_negative;
	char	*res;

	res = malloc(33 * sizeof(char));
	is_negative = (*nbr == '-') ? 1 : 0;
	*res = is_negative ? '-' : 0;
	res = is_negative ? (res + 1) : res;
	nbr = (*nbr == '-' || *nbr == '+') ? (nbr + 1) : nbr;
	if (!is_input_valid(nbr, base_from, base_to))
		return (0);
	b_l = 0;
	while (base_from[b_l])
		b_l++;
	nb_l = 0;
	while (nbr[nb_l])
		nb_l++;
	ft_strrev(to_base_xx(to_base_10(nbr, base_from, b_l, nb_l), base_to, res));
	*res = (!*res) ? base_to[0] : *res;
	return ((is_negative && *res != '0') ? (res - 1) : res);
}
