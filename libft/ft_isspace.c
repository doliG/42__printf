/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isspace.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: glodi <glodi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/08 15:26:18 by glodi             #+#    #+#             */
/*   Updated: 2017/12/09 16:27:50 by glodi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_isspace(int c)
{
	return (c == (int)'\t'
		|| c == (int)'\r'
		|| c == (int)'\v'
		|| c == (int)'\f'
		|| c == (int)'\n'
		|| c == (int)' ');
}
