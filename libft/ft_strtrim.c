/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: glodi <glodi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/02 16:08:34 by glodi             #+#    #+#             */
/*   Updated: 2017/12/08 13:33:24 by glodi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strtrim(char const *s)
{
	size_t	start;
	size_t	end;
	char	*res;

	if (!s)
		return (NULL);
	start = 0;
	while (s[start]
		&& (s[start] == ' ' || s[start] == '\n' || s[start] == '\t'))
		start++;
	end = ft_strlen(s) - 1;
	while (end > start
		&& (s[end] == ' ' || s[end] == '\n' || s[end] == '\t'))
		end--;
	res = ft_strsub(s, (unsigned int)start, end - start + 1);
	return (res);
}
