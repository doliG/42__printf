/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: glodi <glodi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/07 17:31:12 by glodi             #+#    #+#             */
/*   Updated: 2017/12/08 16:30:28 by glodi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_itoa(int n)
{
	long	nb;
	int		is_neg;
	int		size;
	char	*res;

	if (n == 0)
		return (ft_strdup("0"));
	nb = n;
	is_neg = (nb < 0);
	nb = (is_neg) ? -nb : nb;
	size = is_neg;
	while (n && size++ != -1)
		n /= 10;
	if (!(res = ft_strnew(size)))
		return (NULL);
	res += size - 1;
	while (nb)
	{
		*res = nb % 10 + 48;
		nb /= 10;
		nb && res--;
	}
	if (is_neg)
		*(--res) = '-';
	return (res);
}
