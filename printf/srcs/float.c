/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   float.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: glodi <glodi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/11 10:39:07 by glodi             #+#    #+#             */
/*   Updated: 2018/12/11 19:48:05 by glodi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static void	float_left_to_buffer(t_buff *buff, t_flags *flags, t_conv *conv)
{
	int		i;
	size_t	end;

	i = 0;
	fill_field_width(buff, flags);
	end = buff->curr_idx;
	buff->curr_idx -= flags->min_field_width;
	if (conv->sign)
		add_char(buff, conv->sign);
	while (conv->n_len-- > flags->precision + 1)
		add_char(buff, conv->n_conv[i++]);
	if (conv->n_len > 0 || flags->alternate_form)
		add_char(buff, '.');
	while (conv->n_len-- > 0)
		add_char(buff, conv->n_conv[i++]);
	if (end > buff->curr_idx)
		buff->curr_idx = end;
}

static void	float_to_buffer(t_buff *buff, t_flags *flags, t_conv *conv)
{
	int	i;

	i = 0;
	if (flags->padd_with_zero && conv->sign)
		flags->min_field_width++;
	fill_field_width(buff, flags);
	if (conv->sign && flags->padd_with_zero)
		buff->str[buff->curr_idx - flags->min_field_width] = conv->sign;
	if (conv->n_len + !!conv->sign < flags->min_field_width)
		buff->curr_idx -= conv->n_len + !!conv->sign;
	else
		buff->curr_idx -= flags->min_field_width;
	if (conv->sign && !flags->padd_with_zero)
		add_char(buff, conv->sign);
	while (conv->n_len-- > flags->precision + 1)
		add_char(buff, conv->n_conv[i++]);
	if (conv->n_len > 0 || flags->alternate_form)
		add_char(buff, '.');
	while (conv->n_len-- > 0)
		add_char(buff, conv->n_conv[i++]);
}

static int	f_pow(int n, int pow)
{
	int i;

	i = 0;
	while (i++ < pow)
		n *= 10;
	return (n);
}

void		pf_handle_flt(t_buff *buff, t_flags *flags, t_conv *conv,
		va_list ap)
{
	conv->n_f = va_arg(ap, double);
	if (flags->precision == -1)
		flags->precision = 6;
	conv->n_f = conv->n_f * f_pow(10, flags->precision);
	pf_ftoa_base(conv, 10);
	conv->n_len = ft_strlen(conv->n_conv);
	if (flags->justify_left)
		float_left_to_buffer(buff, flags, conv);
	else
		float_to_buffer(buff, flags, conv);
}
