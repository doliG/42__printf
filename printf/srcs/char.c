/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   char.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: glodi <glodi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/08 16:29:26 by glodi             #+#    #+#             */
/*   Updated: 2018/12/11 19:37:56 by glodi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	pf_handle_char(t_buff *buff, t_flags *flags, t_conv *conv, va_list ap)
{
	int	i;
	int	end_idx;

	i = 0;
	end_idx = 0;
	conv->c = va_arg(ap, unsigned int);
	if (1 < flags->min_field_width)
	{
		fill_field_width(buff, flags);
		end_idx = buff->curr_idx;
		if (flags->justify_left)
			buff->curr_idx -= flags->min_field_width;
		else
			buff->curr_idx -= 1;
	}
	add_char(buff, conv->c);
	if (end_idx)
		buff->curr_idx = end_idx;
}

void	pf_handle_unknown(t_buff *buff, t_flags *flags, t_conv *conv)
{
	int	i;
	int end_idx;

	i = 0;
	end_idx = 0;
	conv->c = flags->conversion;
	if (1 < flags->min_field_width)
	{
		fill_field_width(buff, flags);
		end_idx = buff->curr_idx;
		if (flags->justify_left)
			buff->curr_idx -= flags->min_field_width;
		else
			buff->curr_idx -= 1;
	}
	add_char(buff, conv->c);
	if (end_idx)
		buff->curr_idx = end_idx;
}
