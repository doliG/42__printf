/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ptr.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: glodi <glodi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/10 14:54:02 by glodi             #+#    #+#             */
/*   Updated: 2018/12/11 19:46:26 by glodi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static void	ptr_to_buffer(t_buff *buff, t_flags *flags, t_conv *conv)
{
	int	to_write;
	int i;

	to_write = conv->total_len;
	i = 0;
	if (flags->alternate_form && conv->fill == '0')
	{
		to_write -= add_char(buff, '0');
		to_write -= add_char(buff, 'x');
	}
	while (to_write > conv->partial_len)
		to_write -= add_char(buff, conv->fill);
	if (flags->alternate_form && conv->fill != '0')
	{
		to_write -= add_char(buff, '0');
		to_write -= add_char(buff, 'x');
	}
	while (to_write > conv->n_len)
		to_write -= add_char(buff, '0');
	while (to_write > 0)
		to_write -= add_char(buff, conv->n_conv[i++]);
}

static void	ptr_left_to_buffer(t_buff *buff, t_flags *flags, t_conv *conv)
{
	int	to_write;
	int i;

	to_write = conv->total_len;
	i = 0;
	if (flags->alternate_form)
	{
		to_write -= add_char(buff, '0');
		to_write -= add_char(buff, 'x');
	}
	while (conv->partial_len > conv->n_len)
	{
		to_write -= add_char(buff, '0');
		conv->partial_len--;
	}
	while (conv->n_conv[i])
	{
		to_write -= add_char(buff, conv->n_conv[i]);
		i++;
	}
	while (to_write > 0)
		to_write -= add_char(buff, ' ');
}

void		pf_handle_ptr(t_buff *buff, t_flags *flags, t_conv *conv,
		va_list ap)
{
	if (!flags->length[0])
		flags->length[0] = 'l';
	flags->alternate_form = 1;
	ucast_ap(flags, conv, ap);
	pf_ulltoa_base(flags, conv, 16);
	conv->fill = flags->padd_with_zero && !flags->justify_left
		&& !(flags->precision != -1) ? '0' : ' ';
	conv->n_len = ft_strlen(conv->n_conv);
	if (conv->n == 0 && flags->precision == 0)
		conv->n_len = 0;
	conv->partial_len = pf_greatest(2, flags->precision, conv->n_len);
	if (flags->alternate_form && !flags->justify_left)
		conv->partial_len += 2;
	conv->total_len = pf_greatest(3, flags->min_field_width,
			conv->partial_len, conv->n_len);
	if (flags->justify_left)
		ptr_left_to_buffer(buff, flags, conv);
	else
		ptr_to_buffer(buff, flags, conv);
}
