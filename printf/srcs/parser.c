/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_flags.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: glodi <glodi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/22 16:18:37 by glodi             #+#    #+#             */
/*   Updated: 2018/12/12 15:56:49 by glodi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static char	*analyze_format_flags(char *format, t_flags *flags)
{
	while (*format == '#' || *format == '0' || *format == '-'
			|| *format == '+' || *format == ' ')
	{
		if (*format == '#')
			flags->alternate_form = 1;
		else if (*format == '0')
			flags->padd_with_zero = 1;
		else if (*format == '-')
			flags->justify_left = 1;
		else if (*format == '+')
			flags->print_plus_sign = 1;
		else if (*format == ' ')
			flags->print_invisible_plus_sign = 1;
		format++;
	}
	return (format);
}

static char	*analyze_field_width(char *format, t_flags *flags)
{
	if (ft_isdigit(*format))
	{
		flags->min_field_width = ft_atoi(format);
		while (ft_isdigit(*format))
			format++;
	}
	return (format);
}

static char	*analyze_precision(char *format, t_flags *flags)
{
	flags->precision = -1;
	if (*format == '.')
	{
		format++;
		if (ft_isdigit(*format))
		{
			flags->precision = ft_atoi(format);
			while (ft_isdigit(*format))
				format++;
		}
		else
			flags->precision = 0;
	}
	return (format);
}

static char	*analyze_length(char *format, t_flags *flags)
{
	if (*format == 'l' || *format == 'h' || *format == 'l' || *format == 'j'
		|| *format == 'z')
		flags->length[0] = *format++;
	if (*format == 'l' || *format == 'h' || *format == 'l' || *format == 'j'
		|| *format == 'z')
		flags->length[1] = *format++;
	if (*format == 'l' || *format == 'h' || *format == 'l' || *format == 'j'
		|| *format == 'z')
		flags->length[2] = *format++;
	return (format);
}

/*
** This function parse all the flags after a '%' sign, and return a pointer to
** the format string at the end of formatting.
** Autorized flags are :
** - Length modifiers : 'll', 'hh', 'l', 'h'
** - Format modifiers : '-', '0', '+', ' '
**
** NOTES:
** - Handle small flags like # (?) . space digits.
** - Even if there is no valid conversion flags, format and length apply to
**   next char which are not a flag nor a conversion char.
*/

char		*parse_flags(char *format, t_flags *flags)
{
	format = analyze_format_flags(format, flags);
	format = analyze_field_width(format, flags);
	format = analyze_precision(format, flags);
	format = analyze_length(format, flags);
	flags->conversion = *format;
	if (*format)
		format++;
	return ((char *)format);
}
