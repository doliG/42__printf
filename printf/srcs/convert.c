/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   convert.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: glodi <glodi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/28 13:33:26 by glodi             #+#    #+#             */
/*   Updated: 2018/12/11 18:04:11 by glodi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

/*
** Cast va_arg for signed flags d, i
*/

void	cast_ap(t_flags *flags, t_conv *conv, va_list ap)
{
	intmax_t n;

	n = va_arg(ap, uintmax_t);
	if (flags->length[0] == 'l' && flags->length[1] == 'l')
		conv->n = ((long long)n);
	else if (flags->length[0] == 'l' && flags->length[1] == '\0')
		conv->n = ((long)n);
	else if (flags->length[0] == 'h' && flags->length[1] == 'h')
		conv->n = ((char)n);
	else if (flags->length[0] == 'h' && flags->length[1] == '\0')
		conv->n = ((short)n);
	else if (flags->length[0] == 'j' && flags->length[1] == '\0')
		conv->n = ((intmax_t)n);
	else if (flags->length[0] == 'z' && flags->length[1] == '\0')
		conv->n = ((size_t)n);
	else if (flags->length[2])
		conv->n = ((uintmax_t)n);
	else
		conv->n = ((int)n);
}

/*
** Cast va_arg for unsigned conv ouxX
*/

void	ucast_ap(t_flags *flags, t_conv *conv, va_list ap)
{
	uintmax_t n;

	n = va_arg(ap, uintmax_t);
	if (flags->length[0] == 'l' && flags->length[1] == 'l')
		conv->n = ((unsigned long long)n);
	else if (flags->length[0] == 'l' && flags->length[1] == '\0')
		conv->n = ((unsigned long)n);
	else if (flags->length[0] == 'h' && flags->length[1] == 'h')
		conv->n = ((unsigned char)n);
	else if (flags->length[0] == 'h' && flags->length[1] == '\0')
		conv->n = ((unsigned short)n);
	else if (flags->length[0] == 'j' && flags->length[1] == '\0')
		conv->n = ((uintmax_t)n);
	else if (flags->length[0] == 'z' && flags->length[1] == '\0')
		conv->n = ((size_t)n);
	else if (flags->length[2])
		conv->n = (n);
	else
		conv->n = ((unsigned int)n);
}
