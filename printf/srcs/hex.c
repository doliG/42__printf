/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hex.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: glodi <glodi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/06 15:05:57 by glodi             #+#    #+#             */
/*   Updated: 2018/12/11 19:45:04 by glodi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static void	hex_to_buffer(t_buff *buff, t_flags *flags, t_conv *conv)
{
	int	to_write;
	int i;

	to_write = conv->total_len;
	i = 0;
	if (flags->alternate_form && conv->fill == '0' && conv->n != 0)
	{
		to_write -= add_char(buff, '0');
		to_write -= add_char(buff, flags->conversion);
	}
	while (to_write > conv->partial_len)
		to_write -= add_char(buff, conv->fill);
	if (flags->alternate_form && conv->fill != '0' && conv->n != 0)
	{
		to_write -= add_char(buff, '0');
		to_write -= add_char(buff, flags->conversion);
	}
	while (to_write > conv->n_len)
		to_write -= add_char(buff, '0');
	while (to_write > 0)
		to_write -= add_char(buff, conv->n_conv[i++]);
}

static void	hex_left_to_buffer(t_buff *buff, t_flags *flags, t_conv *conv)
{
	int	to_write;
	int i;

	to_write = conv->total_len;
	i = 0;
	if (flags->alternate_form)
	{
		to_write -= add_char(buff, '0');
		to_write -= add_char(buff, flags->conversion);
	}
	while (conv->partial_len > conv->n_len)
	{
		to_write -= add_char(buff, '0');
		conv->partial_len--;
	}
	while (to_write && conv->n_conv[i])
	{
		to_write -= add_char(buff, conv->n_conv[i]);
		i++;
	}
	while (to_write > 0)
		to_write -= add_char(buff, ' ');
}

void		pf_handle_hex(t_buff *buff, t_flags *flags, t_conv *conv,
		va_list ap)
{
	ucast_ap(flags, conv, ap);
	pf_ulltoa_base(flags, conv, 16);
	conv->fill = flags->padd_with_zero && !flags->justify_left
		&& !(flags->precision != -1) ? '0' : ' ';
	conv->n_len = ft_strlen(conv->n_conv);
	if (conv->n == 0 && flags->precision == 0)
		conv->n_len = 0;
	conv->partial_len = pf_greatest(2, flags->precision, conv->n_len);
	if (flags->alternate_form && conv->n != 0 && !flags->justify_left)
		conv->partial_len += 2;
	conv->total_len = pf_greatest(3, flags->min_field_width,
			conv->partial_len, conv->n_len);
	if (flags->justify_left)
		hex_left_to_buffer(buff, flags, conv);
	else
		hex_to_buffer(buff, flags, conv);
}
