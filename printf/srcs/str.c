/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   str.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: glodi <glodi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/07 16:44:53 by glodi             #+#    #+#             */
/*   Updated: 2018/12/11 19:45:20 by glodi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static void	handle_str_bis(t_buff *buff, t_flags *flags, t_conv *conv,
		int to_write)
{
	int	i;
	int end_idx;

	i = 0;
	end_idx = 0;
	if (to_write < flags->min_field_width)
	{
		fill_field_width(buff, flags);
		end_idx = buff->curr_idx;
		if (flags->justify_left)
			buff->curr_idx -= flags->min_field_width;
		else
			buff->curr_idx -= to_write;
	}
	while (to_write)
	{
		add_char(buff, conv->s[i++]);
		to_write--;
	}
	if (end_idx)
		buff->curr_idx = end_idx;
}

void		pf_handle_str(t_buff *buff, t_flags *flags, t_conv *conv,
		va_list ap)
{
	int	to_write;

	conv->s = va_arg(ap, char*);
	conv->fill = flags->padd_with_zero && !flags->justify_left
		&& !(flags->precision != -1) ? '0' : ' ';
	if (!conv->s)
		conv->s = (char *)&"(null)";
	conv->s_len = ft_strlen(conv->s);
	if (flags->precision != -1 && flags->precision < conv->s_len)
		to_write = flags->precision;
	else
		to_write = conv->s_len;
	handle_str_bis(buff, flags, conv, to_write);
}
