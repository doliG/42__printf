/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fill_field_width.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: glodi <glodi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/08 16:31:33 by glodi             #+#    #+#             */
/*   Updated: 2018/12/11 19:01:18 by glodi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	fill_field_width(t_buff *buff, t_flags *flags)
{
	char	fill;
	int		i;

	i = 0;
	fill = flags->padd_with_zero && !flags->justify_left ? '0' : ' ';
	if (flags->min_field_width > PF_BUFF_SIZE / 2)
		flags->min_field_width = PF_BUFF_SIZE / 2;
	while (i < flags->min_field_width)
	{
		add_char(buff, fill);
		i++;
	}
}
