/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   udec.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: glodi <glodi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/06 14:10:24 by glodi             #+#    #+#             */
/*   Updated: 2018/12/11 19:45:32 by glodi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static void	udec_to_buffer(t_buff *buff, t_conv *conv)
{
	int	to_write;
	int i;

	to_write = conv->total_len;
	i = 0;
	while (to_write > conv->partial_len)
		to_write -= add_char(buff, conv->fill);
	while (to_write > conv->n_len)
		to_write -= add_char(buff, '0');
	while (to_write)
		to_write -= add_char(buff, conv->n_conv[i++]);
}

void		udec_left_to_buffer(t_buff *buff, t_conv *conv)
{
	int	to_write;
	int i;

	to_write = conv->total_len;
	i = 0;
	while (conv->partial_len > conv->n_len)
	{
		to_write -= add_char(buff, '0');
		conv->partial_len--;
	}
	while (conv->n_conv[i])
		to_write -= add_char(buff, conv->n_conv[i++]);
	while (to_write)
		to_write -= add_char(buff, ' ');
}

void		pf_handle_udec(t_buff *buff, t_flags *flags, t_conv *conv,
		va_list ap)
{
	ucast_ap(flags, conv, ap);
	pf_ulltoa_base(flags, conv, 10);
	conv->fill = flags->padd_with_zero && !flags->justify_left
		&& !(flags->precision != -1) ? '0' : ' ';
	conv->n_len = ft_strlen(conv->n_conv);
	if (conv->n == 0 && flags->precision == 0)
		conv->n_len = 0;
	conv->partial_len = pf_greatest(2, flags->precision, conv->n_len);
	conv->total_len = pf_greatest(3, flags->min_field_width,
			flags->precision, conv->n_len);
	if (flags->justify_left)
		udec_left_to_buffer(buff, conv);
	else
		udec_to_buffer(buff, conv);
}
