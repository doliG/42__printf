/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dec.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: glodi <glodi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/05 16:02:17 by glodi             #+#    #+#             */
/*   Updated: 2018/12/11 19:44:42 by glodi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static void	dec_to_buffer(t_buff *buff, t_conv *conv)
{
	int	to_write;
	int	sign_printed;
	int	i;

	to_write = conv->total_len;
	sign_printed = 0;
	i = 0;
	if (conv->sign && to_write > conv->partial_len && conv->fill == '0')
	{
		to_write -= add_char(buff, conv->sign);
		sign_printed = 1;
	}
	while (to_write > conv->partial_len)
		to_write -= add_char(buff, conv->fill);
	if (conv->sign && !sign_printed && to_write > conv->n_len)
		to_write -= add_char(buff, conv->sign);
	while (to_write > conv->n_len)
		to_write -= add_char(buff, '0');
	while (to_write)
		to_write -= add_char(buff, conv->n_conv[i++]);
}

static void	dec_left_to_buffer(t_buff *buff, t_conv *conv)
{
	int	to_write;
	int i;

	to_write = conv->total_len;
	i = 0;
	if (conv->sign)
		to_write -= add_char(buff, conv->sign);
	while (conv->partial_len - !!conv->sign > conv->n_len)
	{
		to_write -= add_char(buff, '0');
		conv->partial_len--;
	}
	while (conv->n_conv[i])
		to_write -= add_char(buff, conv->n_conv[i++]);
	while (to_write)
		to_write -= add_char(buff, ' ');
}

void		pf_handle_dec(t_buff *buff, t_flags *flags, t_conv *conv,
		va_list ap)
{
	cast_ap(flags, conv, ap);
	pf_lltoa_base(flags, conv, 10);
	conv->fill = flags->padd_with_zero && !flags->justify_left
		&& !(flags->precision != -1) ? '0' : ' ';
	if (flags->print_plus_sign && conv->n >= 0)
		conv->sign = '+';
	else if (flags->print_invisible_plus_sign && conv->n >= 0)
		conv->sign = ' ';
	conv->n_len = ft_strlen(conv->n_conv);
	if (conv->n == 0 && flags->precision == 0)
		conv->n_len = 0;
	conv->partial_len = pf_greatest(2, flags->precision, conv->n_len)
		+ !!conv->sign;
	conv->total_len = pf_greatest(3, flags->min_field_width,
			flags->precision + !!conv->sign, conv->n_len + !!conv->sign);
	if (flags->justify_left)
		dec_left_to_buffer(buff, conv);
	else
		dec_to_buffer(buff, conv);
}
