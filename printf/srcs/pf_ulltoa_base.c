/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pf_ulltoa_base.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: glodi <glodi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/06 14:22:53 by glodi             #+#    #+#             */
/*   Updated: 2018/12/11 18:24:51 by glodi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static unsigned long long	ft_abs(long long n)
{
	if (n < 0)
		return (-n);
	return (n);
}

/*
** This function return the len of n converted into base and as a char*
*/

static unsigned int			pf_numlen_base(unsigned long long n, int base)
{
	int	len;

	len = 1;
	while (n /= base)
		len++;
	return (len);
}

/*
** This is a custom function. Instead of return the answer, it'll write
** it in `t_conv`
** Note that it'll handle hexa depending of x / X flag.
*/

void						pf_ulltoa_base(t_flags *flags, t_conv *conv,
		int base)
{
	char					*str_base;
	int						len;
	unsigned long long		n;

	n = conv->n;
	str_base = flags->conversion == 'X' ? STR_BASE_UP : STR_BASE_LO;
	len = pf_numlen_base(n, base);
	while (len > 1)
	{
		conv->n_conv[len - 1] = str_base[ft_abs(n % base)];
		n /= base;
		len--;
	}
	conv->n_conv[len - 1] = str_base[ft_abs(n % base)];
}
