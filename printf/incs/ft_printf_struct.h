/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf_struct.h                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: glodi <glodi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/11 19:21:27 by glodi             #+#    #+#             */
/*   Updated: 2018/12/11 19:33:46 by glodi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_STRUCT_H
# define FT_PRINTF_STRUCT_H

# define PF_BUFF_SIZE 1024

typedef struct	s_buff
{
	size_t		curr_idx;
	size_t		char_written;
	int			fd;
	char		str[PF_BUFF_SIZE];
}				t_buff;

typedef struct
{
	int			alternate_form;
	int			padd_with_zero;
	int			justify_left;
	int			print_plus_sign;
	int			print_invisible_plus_sign;

	int			precision;
	int			min_field_width;

	char		conversion;
	char		length[3];
}				t_flags;

typedef intmax_t	t_i_max;
typedef struct	s_conv
{
	t_i_max		n;
	double		n_f;

	char		*s;
	int			s_len;
	int			max_len;

	char		c;

	char		sign;
	char		fill;
	char		n_conv[65];

	int			n_len;
	int			partial_len;
	int			total_len;
}				t_conv;

#endif
